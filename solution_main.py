from animation import *
from monsters import *
from customException import *
import random

class Main():
    def __init__(self):
        try:
            player_name = input("Hey, gimme yer name (A-Z, a-z): ")
            age = input("Now tell me, how many years you live in Earth (int): ")
            pet_name = input("I wanna give u a pet, name it (A-Za-z): ")
            opp_name = input("Whats the thing u hate most, name it (A-Za-z): ") 

            if not(player_name.isalpha() and pet_name.isalpha() and age.isnumeric() and opp_name.isalpha()):
                raise InvalidInputError

            monster = random.randint(0,2)
            pet = None
            opponent = None
            age = int(age)

            '''
            Initiate the class, make a new Object of the class, given that:
            age = your pet level (age from input)
            pet_name = your pet name (pet_name from input)
            '''
            if monster == 0:
                # TODO: PET == monster GHOST
                # TODO: OPPONENT == monster JUGGERNAUT or OPPONENT == monster ROBBER
                '''
                clue:
                monster = random.randint(0,2)
                if monster == 0:
                else:
                '''
                monster = random.randint(0,2)
                pet = Ghost(name=pet_name, lvl=age, role="team")
                if monster == 0:
                    opponent = Juggernaut(name=opp_name, lvl=age, role="opponent")
                else:
                    opponent = Robber(name=opp_name, lvl=age, role="opponent")
            else:
                # TODO: PET == monster JUGGERNAUT

                # TODO: OPPONENT == monster GHOST or OPPONENT == monster ROBBER
                monster = random.randint(0,2)
                pet = Juggernaut(name=pet_name, lvl=age, role="team")
                if monster == 0:
                    opponent = Juggernaut(name=opp_name, lvl=age, role="opponent")
                else:
                    opponent = Robber(name=opp_name, lvl=age, role="opponent")
            

            print("WARNING!!!\nYou are about to enter the arena, " + player_name + "!")
            print('''
            =============================================
                        WELCOME TO THE ARENA!
            =============================================\n
            ''')
            
            # TODO: Initiate Animation class here
            animation = Animation(pet, opponent)
            animation.showFigure(pet)
            animation.showFigure(opponent)
            while(pet.isAlive() and opponent.isAlive()):
                # TODO: implement game
                '''
                CLUE: read thoroughly class animation and class monsters, understand it, and use it!

                this is important, READ and UNDERSTAND both Animation dan Monsters class!
                and ONLY after that do this part!
                '''
                animation.animateAttack(pet)
                status = pet.attack(opponent)
                if status[1]:
                    print(pet_name + " got a critical hit on " + opp_name)
                print(pet_name + " dealt " + str(status[0]) + " damage "+ opp_name)
                animation.animateMoveBack(pet)

                if not (opponent.isAlive()):
                    break

                animation.animateAttack(opponent)
                status = opponent.attack(pet)
                if status[1]:
                    print(opp_name + " got a critical hit on " + pet_name)
                print(opp_name + " dealt " + str(status[0]) + " damage "+ pet_name)
                animation.animateMoveBack(opponent)

                if not (pet.isAlive()):
                    break

            print("\n==============================================================")

            if pet.isAlive():
                print("Well done, " + player_name + " together with " + pet_name + ", You dominate the arena!")
                print("Congratulations on winning!")
            else:
                print("Well, what a noob you are!")
                print("Get out of my way!")
            
            print("==========================G'Bye================================")
            

        except InvalidInputError:
            print("You entered wrong input!")


            




if __name__ == "__main__":
    Main()